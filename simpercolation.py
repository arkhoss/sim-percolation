#!/usr/bin/python3
# Usage: simpercolation.py [percolation number] <args>
# Example: for i in 0.2 0.3 0.4; do python3 simpercolation.py --percolation ${i} --size 10; done
# Author: Kelly Munoz <kellyohana10@gmail.com>
# Version: 1.0
# Description: simulate a percolation matrix given a percolation number

import sys
import random
import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import measurements

#############
# Main Code #
#############
def percolation( p, L ):

    N = L * L            # Matrix size
    W = p * N            # Ocupation Number

    # Matrix
    m = (L+1,L+1)
    M = np.zeros(m)

    # counter
    cont = 0

    # loop until counter is equal to Ocupation Number
    while cont < W:
        # walk the matrix
        x = range(L)
        h=list(x)
        i=random.choice(h)
        j=random.choice(h)
        if int(M[i][j]) == 0:
            # get random number between 0 and 1
            r = random.uniform( 0, 1)
            if r <= p:
                M[i][j] = 1
                cont += 1
    return cont, W, M

#Cluster maximum
def clustermaximum (p, L, M):
    lw, num = measurements.label(M)
    area = measurements.sum(M, lw, index=np.arange(lw.max() + 1))
    cmax=max(area)
    #Order parameter
    Q=cmax/(L*L)
    return lw, num, cmax, area, Q


def runner(L, i, debug):
    lis = []
    for p in np.arange(0.0, 1.0, i):
        # Execute percolation function
        cont, W, M = percolation( p, L )
        lw, num, cmax, area, Q = clustermaximum(p, L, M)

        if debug == "true":
            print("Counter:", cont)
            print("Percolation Number:", p)
            print("Matrix Size:", L)
            print("Ocupation:", W)
            print("Matrix")
            print(M)
            print ("Clusters:",num)
            print (lw)
            print ("Size clusters:",area)
            print("Cluster maximum:",cmax)
            print("Order parameter:", Q)

        lis.append((p, Q))

    return lis



if __name__ == "__main__":

    # execute code inside try block to capture any system error
    try:
        # parser to get arguments from command line
        parser = argparse.ArgumentParser("simulate a percolation matrix given a percolation number")
        parser.add_argument( '-i', '--interval', dest='i', default='0.1', action='store', metavar='', help='Percolation Interval Number' )
        parser.add_argument( '-d', '--debug', dest='d',  default='False', action='store', metavar='', help='Debug Mode' )
        parser.add_argument( '-L', '--size', dest='L',  default='10', action='store', type=int, nargs='+', metavar='N', help='Matrix constant' )
        
        # saver arguments in args
        args = parser.parse_args()

        # assigned value of p and N to variables
        i = float(args.i)
        L = args.L
        debug = str(args.d).lower()

        for s in L:
            s = int(s)
            print("este valor de s:", s)
            lis = runner(s, i, debug)
    
            print(lis)
            
            x_val = [x[0] for x in lis]
            y_val = [x[1] for x in lis]

            print("xval: ", x_val)
            print("yval: ", y_val)
            
            plt.plot(x_val,y_val)
            plt.plot(x_val,y_val,'or')
        
        plt.ylabel('Q')
        plt.xlabel('p')
        plt.suptitle('Percolation Graphics')
        plt.show()
    except SystemExit:
        sys.exit(2)
