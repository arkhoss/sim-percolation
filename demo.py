#!/usr/bin/python3
# Usage: simpercolation.py [percolation number] <args>
# Example: for i in 0.2 0.3 0.4; do python3 simpercolation.py --percolation ${i} --size 10; done
# Author: Kelly Munoz <kellyohana10@gmail.com>
# Version: 1.0
# Description: simulate a percolation matrix given a percolation number

import random

cont = 0
W = 100
N = 10



while cont < W:
    # walk the matrix
    x = range(N)
    l = list(x)
    i = random.choice(l)
    j = random.choice(l)

    print(i)
    print(j)
    cont += 1


print(cont)



def find_1( x ):
    for i in [1, 2, 1, 4, 5]:
        if i == 1:
            print("found")

